db.runCommand({collMod:"clientes", 
    validator:{
        $jsonSchema:{
            bsonType:"object",
            required:["nome","cpf","status_civil","data_nascimento","endereco"],
            properties:{
                nome:{
                    bsonType:"string",
                    description:"Informe corretamente o nome do cliente"
                },
                cpf:{
                    bsonType:"string",
                    description:"Informe corretamente o cpf do cliente"
                },
                status_civil:{
                    bsonType:"string",
                    enum:["Solteiro(a)","Casado(a)","Separado(a)","Divorciado(a)","Viúvo(a)","União estável"],
                    description:"Informe corretamente o status civil do cliente"
                },
                data_nascimento:{
                    bsonType:"string",
                    description:"Informe corretamente a data de nascimento do cliente"
                },
                endereco:{
                    bsonType:"string",
                    description:"Informe corretamente o enderço do cliente"
                }
            }
        }
    },
    validationAction:"warn"
})