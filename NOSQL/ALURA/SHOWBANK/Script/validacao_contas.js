db.runCommand({collMod:"contas",
    validator:{
        $jsonSchema:{
            bsonType:"object",
            "additionalProperties":false,
            required:["_id","Numero_Conta", "Tipo","CPF","Valor","Agência"],
            properties:{
                _id:{
                    bsonType:"objectId",
                    description:"Informe corretamente o endereço do cliente"
                },
                Numero_Conta:{
                    bsonType:"string",
                    description:"Informe corretamente o número da conta do cliente"
                },
                Tipo:{
                    bsonType:"string",
                    enum:["Conta corrente", "Conta poupança", "Conta salário"],
                    description:"Informe corretamente o tipo de conta do cliente"
                },
                CPF:{
                    bsonType:"string",
                    maxLength:14,
                    minLength:14,
                    description:"Informe corretamente o cpf do cliente"
                },
                Valor:{
                    bsonType:"double",
                    description:"Informe corretamente o valor da conta do cliente"
                },
                Agência:{
                    bsonType:"string",
                    description:"Informe corretamente a agência do cliente"
                }
            }  
        }
    },
    validationLevel:"moderate",
    validationAction:"warn"
})