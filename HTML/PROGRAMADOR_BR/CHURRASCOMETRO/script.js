let inputAdultos   = document.getElementById("adultos");
let inputCriancas  = document.getElementById("criancas");
let inputDuracao   = document.getElementById("duracao");
let resultado      = document.getElementById("resultado");

function calcular() {
    let adultos = inputAdultos.value;
    let criancas = inputCriancas.value;
    let duracao = inputDuracao.value;

    let qtdTotalCarne = carnePP(duracao) * adultos + (carnePP(duracao)/2 * criancas);
    let qtdTotalCerveja = cerveja(duracao) * adultos;
    let qtdTotalBebida = refri(duracao) * adultos + (refri(duracao)/2 * criancas);

    resultado.innerHTML = `<p>${qtdTotalCarne/1000} kg de Carne</p>`
    resultado.innerHTML += `<p>${Math.ceil(qtdTotalCerveja/355)} latas de Cerveja</p>`
    resultado.innerHTML += `<p>${Math.ceil(qtdTotalBebida/2000)} garrafas de Refrigerante</p>`

}

function carnePP(){
    if (duracao >= 6){
        return 600;
    }
    else{
        return 400;
    }
}

function cerveja(){
    if (duracao >= 6){
        return 2000;
    }
    else{
        return 1200;
    }
}

function refri(){
    if (duracao >= 6){
        return 1500;
    }
    else{
        return 1000;
    }
}
